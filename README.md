# Guide

This is just a simple chat app that uses Node.js, Express, Socket.io that I followed a Node.js course on Linkedin. Just for practicing.

Packages used:
- nodemon (dev) - for running the server, auto reloading
- request (dev) - for testing request
- jasmine (dev) - for testing
- socket.io - for chatting
- body-parser - for reading json params & urlencoded params
- http - for using socket io along with express
- express - for server
- mongoose - db, for storing messages


Here are steps that you can run this app.

1. Clone it
2. Go into the project `cd /path/to/project`
3. Run `yarn install` to install all the required dependencies
4. Create a mongo db cluster on `https://mongodb.com`
5. Create `.env` file and fill `DB_USER`, `DB_PASSWORD`, `DB_NAME`
6. Install nodemon in global if you don't have it yet: `sudo npm i nodemon -g`
7. To start the application: Run `yarn start`
8. To test the application: Run `yarn test`

Enjoy coding!
- Harefx
